public with sharing class nmAnnouncementsController {

    @AuraEnabled
    public static List<Announcements__c> getAnnouncements(String announcementType){
        List<Announcements__c> announcementsList = new List<Announcements__c>();
        announcementsList = [SELECT Id,Name,Announcement_Type__c,Title__c,Body__c,Active__c,Announcement_Date__c,Hyperlink__c,(SELECT ContentDocumentId, ContentDocument.Title, ContentDocument.ContentSize 
        FROM ContentDocumentLinks) 
         FROM Announcements__c 
         WHERE Active__c = true AND Announcement_Type__c  = :announcementType
         order by Announcement_Date__c desc];
       
        return announcementsList;
    }
 
    @AuraEnabled
    public static String getContentVersionData(String contentDocumentId){
        List<ContentVersion> contentVersionList = [SELECT Id,VersionData,FileType,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: contentDocumentId];  
        return contentVersionList[0].Id;
    }
}