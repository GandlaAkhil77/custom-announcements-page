import { LightningElement, track,api } from "lwc";
import getAnnouncements from "@salesforce/apex/nmAnnouncementsController.getAnnouncements";
import getContentVersionData from "@salesforce/apex/nmAnnouncementsController.getContentVersionData";
import nm_covid_design from "@salesforce/resourceUrl/nm_covid_design";
import NM_DOH_Logo from '@salesforce/resourceUrl/NM_DOH_Logo';
import {
	loadStyle
} from "lightning/platformResourceLoader";

import {
	showToast,
	isValid,
  formattedDateDay
} from 'c/pubsub';

export default class NmAnnouncementsPage extends LightningElement {
  @track newsList = [];
  @track imgLink = "";
  @track ContentDocumentId;
  @track ContentVersionId;
  @track pageSizeOptionsVals = [5, 10, 15, 20];
  @track perpage = 5;
  @track initNumber;
  @track lastNumber;
  @track visibleList = [];
  @track callPagination = false;
  @api announcementType;
  @track showSpinner = false;
  

 
  connectedCallback() {
		loadStyle(this, nm_covid_design + "/sass/main.css");

    this.getAnnouncementsData();

    let v = window.localStorage.getItem('nmAnnouncementsPage');
		if(v){ 
			this.perpage = v;
		}

  }

  setPageSize(event) {
		this.perpage = event.detail;
		window.localStorage.setItem("nmAnnouncementsPage", this.perpage);
  }
  
  handlePagination(event) {
		this.page = event.detail.currentPage;
		this.initNumber = event.detail.initNumber;
		this.lastNumber = event.detail.lastNumber;
    this.setVisibleList(event.detail.initNumber, event.detail.lastNumber);
	}

  setVisibleList(initNumber, lastNumber) {
		var tempList = [];
    tempList = this.newsList;
    this.visibleList = tempList.slice(initNumber, lastNumber);
  }
  
  updateTable() {
		
		this.setVisibleList(this.initNumber, this.lastNumber);
	}
 


  getAnnouncementsData() {
    this.showSpinner = true;
    getAnnouncements({
      announcementType: this.announcementType
			})
      .then(result => {
        console.log("Result::" + JSON.stringify(result));
        let temparray = [];
        result.forEach(element => {
          
          let _element = Object.assign({}, element);
          _element.imgLink = "";
 
          if (element.ContentDocumentLinks == undefined) {
            this.imgLink = "";
            _element.imgLink = NM_DOH_Logo;
            _element.cid = "";
          } else {
            
            let size = element.ContentDocumentLinks.length;
            _element.cid = element.ContentDocumentLinks[size - 1].ContentDocumentId;
            //_element.cid = element.ContentDocumentLinks[0].ContentDocumentId;
          }
          // _element.DateVal = new Date(element.Announcement_Date__c).toDateString("en-US", {
          //   timeZone: "America/Phoenix"
          // });
          _element.DateVal = element.Announcement_Date__c;
          
          _element.newsUrl = element.Hyperlink__c;
          element = { ..._element };
          temparray.push(element);
 
        });
        this.newsList = temparray;
        this.getContentVersionId();
      })
      .catch(error => {
        this.showSpinner = false;
        console.log("Error while fetching news " + error);
      });
  }

  getContentVersionId() {
    this.newsList.forEach(element => {
      if (element.cid != "") {
        setTimeout(function () {
          getContentVersionData({ contentDocumentId: element.cid })
            .then(result => {
             
              this.ContentVersionId = result;
              element.imgLink =
                "/sfc/servlet.shepherd/version/download/" + this.ContentVersionId;
            })
            .catch(error => {
              
              console.log("Error while fetching contentversion id " + error);
            });
        }.bind(this), 200);
      }
    });
    this.showSpinner = false;
 
  }
  
}